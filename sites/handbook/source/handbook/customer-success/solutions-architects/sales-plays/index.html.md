---
layout: handbook-page-toc
title: "Sales Plays"
description: “Discover GitLab Solutions Architects’ Sales Plays practices”
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes) - [**Education and Enablement**](/handbook/customer-success/education-enablement)
 
## Sales Plays for new account & teams(existing customer) lands
 
The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges  information uncovered by prior interactions with the account.  Occasionally, SA's may support a combination call of discovery and technical demonstration/deep-dive on a single call, but this is suboptimal as the latter approach does not allow the SA time to prepare for and/or tailor the discussion.
 
The SA is also responsible for any pre-sales technical customer inquiry or audit from associated accounts, including RFI, RFP or security audits. For details on the audit process, proceed to the [Security](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html) page.
 
The SA is always responsible for drafting Professional Services SOW's, regardless of the account status (pre- and post-sales).
 
As a general guideline, no more than 2 Solutions Architects should join any single client-facing call, whether it be assistive for the topic or for learning / shadowing.
 
## Overview
 
**Objective** - Land new accounts, logos & new teams in existing account with Gitlab Ultimate.
 
Who is this sales play for? 
* Primary: SALs, SDRs and SAs who handle new accounts, new teams in existing customers 
* Secondary: TAMs who help in discovery with new teams in existing customers 
 
 
## Who to meet
{: .alert .alert-gitlab-orange}
 
**Ideal Customer Profile**  - Prospects or new teams in existing customers who are listed in this qualification [list](https://gitlab.my.salesforce.com/00O4M000004aULR) for enterprise customers
  - Organizations/teams interested in shifting left & currently unhappy with their security tools OR not using security tool at all
  - Organizations/teams unhappy with their deploy times, current operational efficiency, tool chain
  - Organizations at lower maturity level undergoing or planning to undergoing tranformation
  - Siloed teams with disparate tools, especially security, lacking visibility at the individual level and executive level
  - Organizations with stringent regulatory security or compliance requirements
 
**Target Buyer Personas**
 
| Persona role  | Possible titles|
| ------------- | ---------------------- |
| Economic buyers    | CISO or Security Manager, VP of Security, Director of Security, VP of IT or CTO, App/Dev Director |
| Technical influencers    | Chief Architect, App Dev Manager |
| Other Personas to consider | Infrastructure Engineering Director, Release and Change Management Director |
 
 
**Target Account Lists**
 
- [Account list](docs.google.com/spreadsheets/d/1Wb1oumCp8vA7hwexQ-bKm37mDbj4rzwpLJ11TtJDh98/edit#gid=1408744602)
 
## Getting Started
 
Consider the following questions:
* What has prevented the customer from moving to (or considering moving to) GitLab Ultimate?
* Does the customer have any strategic initiative or priority to which upgrading to Ultimate would align nicely?
* Are you engaged with the right personas/teams (see Target Buyer Personas above)?
* Do you have access to power/authority (a business decision maker)?
* Who are your champions within the account?
* Are the capabilities and PBOs that are enabled by GitLab Ultimate important to the customer? Why or why not? How do you know?
 
### Qualification
 
1. For SALs/SDRs please make sure the customers are on the qualification list for your region. Eg: For enterprise west look at [this](https://gitlab.my.salesforce.com/00O4M000004aULR)
2. During qualification, make sure to identify the pain points, customer profile using the collaterals highlighted below:
   1. [Questions](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#engaging-the-customer) to better understand customer needs, future state & required capabilities
   2. Customer presentations & GTM use cases link [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/#customer-facing-presentations)
   3. [Discovery questions](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#discovery-questions) for Gitlab Protect/Secure
   4. Potential [objections](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#objection-handling) from prospects/customers
 
3. Review how to position gitlab's values & why does gitlab does it better [here](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#positioning-value)
 
### Discovery
 
1. New Customers(new logos)
   1. Choose & create a list of discovery questions based on your customer profile in order to identify pain points
       1.Discover the pain points through general discovery questions
           1. SA technical discovery [questionaire](https://docs.google.com/document/d/1GZnbqE_rtGFRAJxoeje4mtFyP1AfpLLueESijXgikm4/edit#heading=h.4b6ktib5prk9)
           2. Market [requirements](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#market-requirements-in-priority-order)
           3. Pick the ones that apply from value discovery pains, benefits & required capabilities[here](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#value-discovery)
       2. Discovery questions for customer currently not on secure/protect with specific entry points [highlighted](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#discovery-questions)
       3. Please review potential [objections](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#objection-handling)from prospects/customers before the meeting
   2. Identify & get access to high-level DRI for security</span>
   3. Identify & get access to high-level exec for higher-level initiatives for example: Gitlab is optimized for [cloud native](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit#slide=id.gbb4f272403_0_546)
   4. Propose next steps & get some time while you are speaking with the customer ideally a demo or workshop. We DONOT recommend starting with a POV since it leads to longer sales cycles & unexpected results before doing more discovery & agreeing to success criterias.
 
2. New teams(existing customers)
   1. Choose & create a list of discovery questions based on your customer profile in order to identify pain points
       1.Discover the pain points through general discovery questions
           1. SA technical discovery [questionaire](https://docs.google.com/document/d/1GZnbqE_rtGFRAJxoeje4mtFyP1AfpLLueESijXgikm4/edit#heading=h.4b6ktib5prk9)
           2. Market [requirements](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#market-requirements-in-priority-order)
           3. Pick the ones that apply from value discovery pains, benefits & required capabilities[here](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#value-discovery)
 
       2. Discovery questions for customer currently not on secure/protect with specific entry points [highlighted](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#discovery-questions)
       3. Please review potential [objections](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#objection-handling)from prospects/customers before the meeting
       4. Please review all the this [sales play](https://about.gitlab.com/handbook/marketing/sales-plays-cicd/playbook-premium-to-ultimate/#meetings-to-get-to-a-pov-agreement) if your customer is already on Premium
       5. Identify the director or DRI for secure within the existing customer
       6. Identify some(smaller) teams within existing customers that will be willing to use Ultimate, so as to provide them with multi-tier pricing.
       7. Identify & get access to high-level exec for higher-level initiatives for example: Gitlab is optimized for [cloud native](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit#slide=id.gbb4f272403_0_546)
       8. Propose next steps & get some time while you are speaking with the customer ideally a demo or workshop. We DONOT recommend starting with a POV since it leads to longer sales cycles & unexpected results before doing more discovery & agreeing to success criterias.
 
### Technical Evaluations
 
1. Strategize with your sales team on which play would you like to run & agree on the expected outcome before the meeting. We dont recommend starting a technical evaluation with a POV.
   * Demo/Deck: Vision & transformation play, where you would likely focus on Gitlab holistically
       1. [Customer facing slides](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit)
       2. [Why choose ultimate](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#ultimate)
       3. [Customer proof point](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#proof-points---customers)
       4. [How Gitlab meets market requirements](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#how-gitlab-meets-the-market-requirements)
       5. [Differentiators](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#top-differentiators) - Feel free to pick as many you would like
       6. [SA Demo Repo](https://gitlabdemo.com/catalog/libraries/3c68d4b8) : break this down into multiple play
       7. [Demo catalog](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog)
       8. [End to End TCO planning workshop](https://docs.google.com/spreadsheets/d/1wVghmLv3E_IKs7rz-quc6jtjZW0p-u3h4iSaDqmm1Nc/edit#gid=467701643), Recording
       9. [End to end demo with secure]
   * Nuts/Bolts play where you will focus on specific secure/ultimate capabilities
      1. [DevSecops Nuts & Bolts deck] by Darwin Sanoy (https://docs.google.com/presentation/d/1NSowoLzyrb_8ACnxwbQ2FcqaoDmavaedUrd2rVidehw/edit#slide=id.gc2a4740e21_0_554)
      2. [Building a continuous remediation habit](https://drive.google.com/file/d/1Gd7pb_-5V9jjJqT7zFNLpxoKjZuOFE63/view)
      3. [Additional info including TCO workshop](https://gitlab.com/gitlab-com/customer-success/tko/nuts-and-bolts-devsecops-motion-feedback)
   * Demo/Deck: Integrate vs Compete play
      1. [Compete : Argo CD/TeamCity](https://gitlabdemo.com/catalog/libraries/categories/ac509772)
      2. [Gitlab advantages over competitors](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#competitive-comparison)
*Expected Outcomes
   1. Hands Workshop
   2. Deeper dive on a particular topic
   3. POV
   4. [Pilot/Professional Service](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit#slide=id.g2823c3f9ca_0_9)
 
 
## Additional Resources to use
 
Will have specific resources under actions above. This is for additional resources.
 
* [GitLab Ultimate specific features and value handbook page](https://about.gitlab.com/pricing/ultimate/#ultimate-specific-features)
* [SKO Expanding to Ultimate slide deck](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit#slide=id.gb4749ff26b_0_85)
* [Why GitLab Ultimate slide deck](https://docs.google.com/presentation/d/1TP5cXH5Nr0VkH7mE6M_-DFXT_Jnq7o5LPxuMUz2paI4/edit?usp=sharing)
* [Selling DevSecOps resource page](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#ultimate)
* Selling security (need link) see John Blevin's online classroom material
* New Gartner MQ for Plan (Expected in May)
* Gartner MQ for AST (expected in May 2021)
 
 
## Measuring progress
{: .alert .alert-gitlab-orange}
 
Consider these milestones and adjust stages in SDLC as you progress.
 
**Milestones**
- [ ] Gameplan with GitLab champion (MEDDPPICC)
- [ ] Meeting with Security team or other economic buyer
- [ ] Agreement to do technical evaluations
- [ ] Technical evaluations requirements defined
- [ ] Successful technical evaluation
- [ ] Proposal
 
 
**Metrics:**
- [ ] Avg days per stage to progress
- [ ] Longest step (common blockage?)
- [ ] Economic buyer titles - common factor?
- [ ] Retro on sales play
