---
layout: handbook-page-toc
title: Professional Service Operations
category: Internal
description: "Learn about the GitLab Professional Services operations processes and workflows."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to GitLab Professional Services Operations 
Professional Services Operations is a collaboration of our consulting, training, schedululing, reporting and backend processes to support the Professional Services team.

#### How to contact us

Our Team Slack Channel [#professional-services](https://gitlab.slack.com/archives/CFRLYG77X) 

The Project Coordinators can be reached by tagging the group `@ps-scheduling`

#### Who we are

[Donita Farnsworth, our Senior Consulting Project Coordinator](https://about.gitlab.com/company/team/#dfarnsworth04)

[Wakae McLaurin, our Senior Training Project Coordinator](https://about.gitlab.com/company/team/#wmclaurin) 

## Project Coordination- Consulting

#### Consulting Project Assignment

When the PC and PM have the project team aligned the PC will send [Consulting Project Assignment](https://docs.google.com/document/d/1HfIt30ksOlhv3zAxh8ZPX-5J59maiuhNgZT2E5lMpsk/edit) in the Mavenlink project activity, this allows the team to be aware of who will be working on the project.

#### Consulting Projects Billing Guidelines

Project billing is outlined in each customer SOW or Order Form.  The current billing terms that Professional Services follows is the following:
   * Billed upon SOW execution
   * Order Form execution
   * Time and Materials
   * Project miletone 
   * Billed half up front and half at project completion
   * Billed at completion
   
Passive acceptance of 5 days is included in the SOW unless different terms are negotiated by the customer and approved by the Director of Professional Services.
   
#### Consulting Projects Revenue Forecasting Guidelines

Project revenue release is followed dependant on project billing type:
   * Billed upon SOW execution billing terms
     * SOW contains milestones that include activities, and revenue can be released as each milestone is                 accepted or passive acceptance is reached
   * Billed upon Order Form execution billing terms
     * SOW contains milestones that include activities, and revenue can be released as each milestone is                 accepted or passive acceptance is reached
   * Time and Materials billing terms
     * Approved time sheet hours reported at each month end 
   * Project milestone SOW billing terms
     * SOW contains milestones that include activities, and revenue can be released as each milestone is                 accepted or passive acceptance is reached
   * Billed half up front and half at project completion SOW billing terms
     * SOW contains milestones that include activities, and revenue can be released as each milestone is                 accepted or passive acceptance is reached
   * Billed at completion billing terms
     * SOW contains milestones that include activities, and revenue can be released as each milestone is                 accepted or passive acceptance is reached

#### How To Forecast Revenue For Consulting Projects

##### T&M Projects
T&M project revenue is forecasted by scheduled soft or hard allocations in the Master Planning in Mavenlink.  The hours that are schedule are multiplied by the rate card on the project.  

##### Fixed Priced Projects

Fixed priced projects are forecasted by the project milestones in the Mavenlink project.  Each milestone has a sign off task, that task is updated with correct sign off date for the activities in the milestone.  Best practice is update the sign off task to give time for the customer to review and obtain acceptance from the customer.
If there is not confidence that the activities will be complete and the customer will sign off, then the Sign Off task should be moved out to the next quarter.

## Project Coordination- Training

#### Training Planning and Scheduling

1: **Closed Order Notification** - Project Coordinators receive an automated Salesforce.com email notification when an order has closed and the opportunity is in a closed won stage.  The Project Coordinator will review the purchased training documentation in Salesforce by reviewing the Quote Charge Summaries section within the Saleforce opportunity and attachments and update the corresponding Mavenlink project per the [Mavenlink Project Creation](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/professional-services-operations/mavenlink-processes/#mavenlink-project-creation) process. 

2: **GitLab.com Professional Services epics** are used for tracking training-related information and checklist including the following:
   * Training dates
   * Class times
   * Assigned instructor
   * Teleconferencing set up
   * Preparation link
   * Send out customer follow up email with class details
   * Add attendance roster to Mavenlink
   * Send out post training email and recording information
   * Close out the project

**Scenario 1** - If a customized training is purchased, then a GitLab.com Professional Services epic should already exist as part of the scoping process.
   * Go to the epic, and update the training course listing section by removing any courses that were not purchased.  Your remaining list should only include the courses that were purchased.
   * You will also need to update the epic name to be the same name as the corresponding Mavenlink project name.
   * Update the Engagement SSOT table and Project Checklist sections based on the opportunity and project details.

**Scenario 2** - If only standard training is purchased, then a GitLab.com Professional Services epic most likely will not already have been created.  Use the [GitLab Services Calculator](https://services-calculator.gitlab.io/) to create an epic.
   * Enter the customer name, your GitLab handle, and your email address into the GitLab Services Calculator and click the **Create PS Scoping Issue** button.  You should see a message that the pipeline is creating.
   * After the epic has been created, you should receive an email notification with the link to the Scope Engagement and Write SoW issue.  Go to the _Scope Engagement and Write SoW_ issue and close it.
   * Go to the epic, and update the training course listing section by removing any courses that were not purchased.  Your remaining list should only include the courses that were purchased.
   * You will also need to update the epic name to be the same name as the corresponding Mavenlink project name.
   * Update the Engagement SSOT table and Project Checklist sections based on the opportunity and project details.

3: Contact the Account Sales representative via the Salesforce opportunity Chatter feed to request customer PoC confirmation.  Use the [example template](https://docs.google.com/document/d/1lr156fdAM24GGWkqpLtkaJ5ofv3uoVx0lC9UDS9m3B0/edit?usp=sharing) to post your message.

4: **Post-Sales Training Intake Questionnaire** - Use the [template](https://docs.google.com/document/d/15Ame5jLudiX-oztJ3padL-syu5g4yKphK4z8WMkfaNE/edit?usp=sharing) to make a copy of the questionnaire for the customer to complete.  Make sure to save the copy in the correct PMO > Active Projects Google Drive folder.  Update the customer’s training intake questionnaire as necessary and add the customer point of contact to the Google doc with edit rights.

5: **Welcome to PS** - Send the Welcome to PS email to the customer per [template](https://docs.google.com/document/d/1rJ9q9gEzsumRxDhoWEe45u70efmKA0eWNg69WONuCYs/edit?usp=sharing).

6: **Training Scheduling** - After receiving customer confirmation and training intake questionnaire feedback, review trainer schedules in Mavenlink and confirm availability.
   * Propose training dates and times to the customer.  Add soft allocations to the Mavenlink project per [Mavenlink Project Creation](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/professional-services-operations/mavenlink-processes/#mavenlink-project-creation) process and to the [PS Schedules Google sheet tracker](https://docs.google.com/spreadsheets/d/1RfjXtliiHmZQQB6OqiOBc9s4U3HSbAv4mJu-o2CfnE0/edit?usp=sharing).
   * Upon receiving the customer's confirmation of training dates and times, change the soft allocations to hard allocations within the Mavenlink project per [Mavenlink Project Creation](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/professional-services-operations/mavenlink-processes/#mavenlink-project-creation) process and update the [PS Schedules Google sheet tracker](https://docs.google.com/spreadsheets/d/1RfjXtliiHmZQQB6OqiOBc9s4U3HSbAv4mJu-o2CfnE0/edit?usp=sharing) accordingly.
   * **Partner Training Work Authorization** - If you are using a partner to deliver the training, create a draft of the applicable Training Work Authorization form and send to the partner for signature using the [example template](https://docs.google.com/document/d/1lr156fdAM24GGWkqpLtkaJ5ofv3uoVx0lC9UDS9m3B0/edit?usp=sharing).
      * After receiving the signed Training Work Authorization form, save a copy in the partner SFDC account and the Professional Services Partner Google Drive folder.
      * Update the partner Statement of Work training funds Google sheet tracker to keep track of the training dollars remaining on the Statement of Work.  Notify the Professional Services Education Services Manager when the funds are getting low so a new Statement of Work and procurement issue can be generated for additional funds.



#### Training Projects Billing Guidelines

Training billing is outlined in each customer SOW or Order Form.  The current billing terms that Professional Services follows is the following:
   * Billed upon SOW
   * Order Form execution
   
Passive acceptance of 5 days is included in the SOW unless different terms are negotiated by the customer and approved by the Director of Professional Services.

#### Training Projects Revenue Forecasting Guidelines

Training revenue release is followed dependant on training billing type:
   * Billed upon SOW 
     * Once each training class is complete and roster is received
   * Order Form execution billing terms
     * Once each training class is complete and roster is received

#### How To Forecast Revenue Training Projects

Training projects are typically considered fixed priced projects and are forecasted by the project milestones in the Mavenlink project.  Each milestone includes tasks for each training course that are used to capture training preparation/planning/closeout and delivery hours.  Each task is updated with the correct due date for the activities in the milestone.  Best practice is to ensure that the task due dates are updated to accurately reflect training completion dates for forecasting purposes.  
If there is not confidence that the activities will be complete, then the task due date should be moved out to the next quarter.

## Operations  

#### Mavenlink Access

To provide Mavenlink access to an internal GitLab team members, provide access by the following:
* Mavenlink Access
  * Settings
  * Members
  * Invite Account Members
* Okta-Mavenlink- Users Google Group
  * Gmail
  * Gmail Apps
  * Groups
  * Okta-Mavenlink- users
  * Members
  * Add Members
 
 To provide Mavenlink access to a GitLab partner, provide access by the following:
* Mavenlink Access
  * Settings
  * Members
  * Invite Account Members
* Process a GitLab Access Request
  * Request Okta
  * Request Mavenlink to be added to Okta

#### Time Tracking 

Accurate time tracking records of hours is essential to ensure revenue can be recognized based upon percentage completion of a project scope as outlined in a Statement of Work ("SOW"), and this data is used in the calculation of gross margin. Key points for time tracking include:

- Best practice is to record time at the end of each day. This provides the most accurate account of hours worked and when it was worked
- Each PSE is required and responsible for tracking their own hours, and submitted weekly by Friday EOD for the week worked
- If time will be worked over the weekend, time sheet should still be submitted by Friday, EOD, then a new line created on the time sheet for hours worked over the weekend
- Billable hours represent work hours that a staff member reports as being aligned to a specific SOW. The format for daily time tracking for each team member is shown below, and is reviewed weekly by PS Operations and Manager
- Hours for PTO, Holidays and Friends and Family day are no longer required to be submitted in the weekly time sheet
- If a team member did not work the time allocated for the week, then hours would be added to the PTO feature in Mavenlink
- Notes are required for the PS Time Tracking Creditable and PS Time Tracking Non Credit projects only, not on customer projects
  
  - PTO should be submitted in the time off feature in Mavenlink, and also follow the company guidelines, [time off process](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)
  - Holidays along with Friends and Family day are scheduled on the Mavenlink calendar
  
- Time is required to round to the nearest quarter hour, example: 
  - 15m should be .25
  - 30m should be .5
  - 45m should be .75

****Mavenlink Internal Projects****

Internal projects are set up to track internal time that is not customer project related. Below is the project name along with tasks and examples.

****PS TimeTracking Creditable****
* SKO (GitLab Sales Kick Off)
* Contribute (GitLab Employee Conference)
* Commit (GitLab User Conference)
* Sales Assistance​
  * All Pre-Sales activities​
  * SOW Creation
* Support ​Assistance
  * Support Cases, if pulled into customer questions after the project is closed
  * Engineering Support
* Practice Development​
  * Creating customer process/documents​
  * Customer templates
  * Customer Articles and/or tools
* Product Development
* Mentoring
* Training Course Grading
   
****PS TimeTracking Non Creditable****
* Knowledge Sharing​
  * Slack​
  * Internal Q&A​
* General Administrative​
  * Time Sheets​
  * Expense Reports​
  * Reviews​
  * HR Items​
  * General Emails​
* Meetings/ Staff Time​
  * Internal Meeting​
  * 1:1s​
  * Weekly Webinars​
  * All Hands​
  * Team Calls​
  * Interviews
* Travel Time
  * Excluding Customer Travel
* Personal Enablement
  * Development​
  * Ramp Up​
  * HR Training
  * Customer Project Shadow

****Customer consulting projects****

  When working on a customer project, all hours worked should be tracked against the project.  Here are some examples: 
  * Project tasks are aligned with SOW activities and hours tracked against the tasks
  * Internal/ Sales Handoff Calls
  * Internal/ External status meeting
  * Support ticket submission while the project is in progress
  * Weekly/ Final customer reports and documentation
  * Status/ Close out customer calls
  * Customer Travel

****Customer training projects****
 
  When working on a training project, all hours worked should be tracked against the project.  Here are some examples:
  * Introduction/ Planning/ Preparation/ Close Out
    * All hours should be tracked against the task for preparation and close out of the training
  * All class hours are to be tracked against the task that gives the name of the training, example is GitLab Basics TRNG Hours

#### Quarter End Time Tracking 
Professional Services has a hard quarterly close for each quarter

Our agreed schedule with finace is [Montly/ Quarterly/ Year End Time Lines](https://docs.google.com/spreadsheets/d/15uTHHnmIvWteYGi98BaikOVDtN99MLiVN9su-YlMMLM/edit#gid=0)

Due to the quarterly close, time sheets will need to be submitted and approved twice in the same week.

Time sheets would be submitted by the team and then approved by the project lead on the cutoff date.  Then an additional project line would be added to finish out the remainder of the week, and follow the normal end of the week process.

Here is an example of a time sheet that has been submitted and approved on a Tuesday, then new lines created for the remainder of the week

![splittimesheet](./splittimesheet.png)


#### Project Expenses
Before making a purchase of any type or booking travel for a customer project, be sure to obtain approval from your Project Manager or Project Coordinator. The Project Manager or Project Coordinator would need to review if an expense requirement was included in the project SOW.  Once the purchase has incurred or travel expenses booked, please, be sure to follow the process outlined to be sure that expenses are accounted for in the month which incurred. 
1. Purchase incurred or travel booked
1. PSE/PM submit expense report through Expensify with PSE Project Tag and COGS as the category
1. PS Manager approves expense report in Expensify
1. PS Manager selects PS Operations as the next approver for the customer expense report
1. PS Operations then reviews the expense report and sends to Finance approval and processing
1. PS Operations then reviews the customer expense report with the assigned Project Manager
1. PS Operations then adds the expense report to the customer project and submits the billing over to Finance if the expense is billable 

The GitLab Billing Manager will pull an expense report after each month end to be sure no expenses were missed during the submission and approval process.

#### Quarterly time tracking entry and approval workflow

* All hours are submitted and approved in Mavenlink on a weekly basis
* The quarterly hours report is pulled from Mavenlink and reviewed by the PC and then provided to the PS Delivery Manager for review and approval
* A PS Delivery Manager will APPROVE the hours, create an issue and attach it to the Time Tracking Epic with the `ManagerCertifiedTimesheet` label
* The approving manager will submit to the Head of PS, [Sr. Director of Professional Services](https://about.gitlab.com/job-families/sales/director-of-professional-services/) for next level approval.  The Head of PS with apply judgement on productive utilization.
* Head of Professional Services will submit to the Professional Services Finance Partner for final approval.

#### Mavenlink Project Status/ Colors

| Mavenlink Status |  |
| ------ | ------ |
| Estimate- Gray | Projects that are tracking Internal time for GitLab PS & GitLab Partners |
| Prospect- Gray | PC is setting up Mavenlink project/ Project is at a Stage 5- start reviewing staffing plan |
| In Set Up- Gray | PC is setting up Mavenlink project/ Reviewing Staffing/ Welcome to PS Email |
| Okay to Start- Light Green | Project setup complete/ PM Planning the project |
| Active- Dark Green | PM/ PSE Actively working the project |
| Closed- Blue | Project work is complete, waiting for billing and revenue to be complete |
| Completed- Blue | Billing and Revenue is complete |
| On Hold- Gray| Project is delayed |
| Backlog- Gray | No work is planned |
| Cancelled- Blue | Project Created but will not be worked for various reasons |


| Mavenlink Project Colors |  |
| ------ | ------ |
| Blue | Training Only Project |
| Yellow | Consulting Only Project |
| Orange | Consulting & Training Project |
| Lime | Internal Project |
  
#### Consulting Project Health Reports

Health Reports provide a weekly snap shot status to PS Management on the overall project status, and rolled up to executives.  The report includes a Red, Yellow and Green indicator along with a section to update on overall status, schedule, scope, budget, client.  Health reports should be filled out by the project lead or project manager by Thursday of week each by EOD for the projects that are in the Active project status.  

* Overall Project Status:
   * Two to three lines describing the overall project status, includes pro/ cons/ blockers
* Project Schedule:
   * Is the project tracking to the current Mavenlink schedule? Y/N and if no, why?
* Project Scope:
   * Is the project tracking to the original scope, as per the SOW? Y/N and if no, why?
* Project Budget:
   * Is the project tracking to the original budget, as per the SOW? Y/N and if no, why?
* Client:
   * How is the customer feeling about the project? Happy, frustrated, engaged, disengaged
   * The project could be status of red for scope, schedule, etc. and the customer is still happy 

#### Mavenlink Reports
The Professional Services Team uses Mavenlink reports to track project and team metrics

[Mavenlink Report List](https://docs.google.com/spreadsheets/d/1AXqpDv5UjuzQQ8nWkEPYhf8JJ1HU08j_DQPdrnlqDKk/edit#gid=0)

### Mavenlink Processes
Mavenlink is our current Professional Services Automation (PSA) system.  Follow the link below to view process steps.

[Link to Mavenlink Processes](/handbook/customer-success/professional-services-engineering/professional-services-operations/mavenlink-processes/)
