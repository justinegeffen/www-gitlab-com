---
layout: handbook-page-toc
title: Product Intelligence Guide
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Intelligence Overview

At GitLab, we collect product usage data for the purpose of helping us build a better product. Data helps GitLab understand which parts of the product need improvement and which features we should build next. Product usage data also helps our team better understand the reasons why people use GitLab. With this knowledge we are able to make better product decisions.

There are several stages and teams involved to go from collecting data to making it useful for our internal teams and customers.

| Stage | Description | DRI | Support Teams |
| ----- | ----------- | --- | ------------- |
| Privacy Settings | The implementation of our Privacy Policy including data classification, data access, and user settings to control what data is shared with GitLab. | Product Intelligence | Legal, Data |
| Collection | <span class="colour" style="color: rgb(0, 0, 0);">The data collection tools used across all GitLab applications including GitLab SaaS, GitLab self-managed, CustomerDot, LicenseDot, VersionDot, and </span><span class="colour" style="color: rgb(17, 85, 204);"><u>[about.gitlab.com](http://about.gitlab.com/)</u></span><span class="colour" style="color: rgb(0, 0, 0);">. Our current tooling includes Snowplow, Usage Ping, and Google Analytics.</span> | Product Intelligence | Infrastructure |
| Extraction | The data extraction tools used to extract data from Product, Infrastructure, Enterprise Apps data sources. Our current tooling includes Stitch, Fivetran, and Custom. | Data |  |
| Loading | The data loading tools used to extract data from Product, Infrastructure, Enterprise Apps data sources and to load them into our data warehouse. Our current tooling includes Stitch, Fivetran, and Custom. | Data |  |
| Orchestration | The orchestration of extraction and loading tooling to move data from sources into the Enterprise Data Warehouse. Our current tooling includes Airflow. | Data |  |
| Storage | The Enterprise Data Warehouse (EDW) which is the single source of truth for GitLab's corporate data, performance analytics, and enterprise-wide data such as Key Performance Indicators. Our current EDW is built on Snowflake. | Data |  |
| Transformation | The transformation and modelling of data in the Enterprise Data Warehouse in preparation for data analysis. Our current tooling is dbt and Python scripts. | Data | Product Intelligence |
| Analysis | The analysis of data in the Enterprsie Data Warehouse using a querying and visualization tool. Our current tooling is Sisense. | Data | Product Intelligence |
[Editable source file](https://docs.google.com/spreadsheets/d/144-BLh7uyX4aY23QNrvke5BqCcb9xfPk2BL4qGFvzFY/edit?usp=sharing)

## Systems overview

The systems overview is a simplified diagram showing the interactions between GitLab Inc and self-managed instances.

![Product Intelligence Overview](product_intelligence_systems_overview.png)

[Source file](https://app.diagrams.net/#G13DVpN-XnhWGz9tqReIj8pp1UE4ehk_EC)

### GitLab Inc

For Product Intelligence purposes, GitLab Inc has three major components:

1. [Data Infrastructure](https://about.gitlab.com/handbook/business-ops/data-team/platform/infrastructure/): This contains everything managed by our data team including Sisense Dashboards for visualization, Snowflake for Data Warehousing, incoming data sources such as PostgreSQL Pipeline and S3 Bucket, and lastly our data collectors [GitLab.com's Snowplow Collector](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/library/snowplow/) and GitLab's Versions Application.
1. GitLab.com: This is the production GitLab application which is made up of a Client and Server. On the Client or browser side, a Snowplow JS Tracker (Frontend) is used to track client-side events. On the Server or application side, a Snowplow Ruby Tracker (Backend) is used to track server-side events. The server also contains Usage Ping which leverages a PostgreSQL database and a Redis in-memory data store to report on usage data. Lastly, the server also contains System Logs which are generated from running the GitLab application.
1. [Monitoring infrastructure](https://about.gitlab.com/handbook/engineering/monitoring/): This is the infrastructure used to ensure GitLab.com is operating smoothly. System Logs are sent from GitLab.com to our monitoring infrastructure and collected by a FluentD collector. From FluentD, logs are either sent to long term Google Cloud Services cold storage via Stackdriver, or, they are sent to our Elastic Cluster via Cloud Pub/Sub which can be explored in real-time using Kibana.

### Self-managed

For Product Intelligence purposes, self-managed instances have two major components:

1. Data infrastructure: Having a data infrastructure setup is optional on self-managed instances. If you'd like to collect Snowplow tracking events for your self-managed instance, you can setup your own self-managed Snowplow collector and configure your Snowplow events to point to your own collector.
1. GitLab: A self-managed GitLab instance contains all of the same components as GitLab.com mentioned above.

### Differences between GitLab Inc and Self-managed

As shown by the orange lines, on GitLab.com Snowplow JS, Snowplow Ruby, Usage Ping, and PostgreSQL database imports all flow into GitLab Inc's data infrastructure. However, on self-managed, only Usage Ping flows into GitLab Inc's data infrastructure.

As shown by the green lines, on GitLab.com system logs flow into GitLab Inc's monitoring infrastructure. On self-managed, there are no logs sent to GitLab Inc's monitoring infrastructure.

Note (1): Snowplow JS and Snowplow Ruby are available on self-managed, however, the Snowplow Collector endpoint is set to a self-managed Snowplow Collector which GitLab Inc does not have access to.

## Collection Framework

![Collection Framework Visual](collection_framework_fy21_q4-update.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

1. Plan-level reporting for Usage Ping Redis on SaaS for multi-tenant reporting
1. Plan-level and Group-level reporting for Usage Ping Postgres on SaaS for multi-tenant reporting
1. Usage Ping Snowplow on Self-Managed (using internal collector and database for moderate volume)
1. Group-level, Project-level, User-level tracking for Snowplow on SaaS
1. Usage Ping Snowplow on SaaS (using external collector and database for scaled up volume)

We use three methods to gather product usage data:

- [Snowplow](#snowplow)
- [Usage Ping](#usage-ping)
- [Database import](#database-import)

### Snowplow

Snowplow is an enterprise-grade marketing and product analytics platform which helps track the way
users engage with our website and application.

Snowplow consists of two components:

- [Snowplow JS](https://github.com/snowplow/snowplow/wiki/javascript-tracker) tracks client-side
events.
- [Snowplow Ruby](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/ruby-tracker/) tracks server-side events.

For more details, read the [Snowplow](https://docs.gitlab.com/ee/development/snowplow/index.html) guide.

### Usage Ping

Usage Ping is a method for GitLab Inc to collect usage data on a GitLab instance. Usage Ping is primarily composed of row counts for different tables in the instance’s database. By comparing these counts month over month (or week over week), we can get a rough sense for how an instance is using the different features within the product. This high-level data is used to help our product, support, and sales teams.

For more details, read the [Usage Ping](https://docs.gitlab.com/ee/development/usage_ping/) guide.

### Database import

Database imports are full imports of data into GitLab's data warehouse. For GitLab.com, the PostgreSQL database is loaded into Snowflake data warehouse every 6 hours. For more details, see the [data team handbook](https://about.gitlab.com/handbook/business-ops/data-team/platform/#extract-and-load).

### Pageview events

- Number of sessions that visited the /dashboard/groups page

### UI events

- Number of sessions that clicked on a button or link
- Number of sessions that closed a modal

UI events are any interface-driven actions from the browser including click data.

### CRUD or API events

- Number of Git pushes
- Number of GraphQL queries
- Number of requests to a Rails action or controller

These are backend events that include the creation, read, update, deletion of records, and other events that might be triggered from layers other than those available in the interface.

### Database records

These are raw database records which can be explored using business intelligence tools like Sisense. The full list of available tables can be found in [structure.sql](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql).

### Instance settings

These are settings of your instance such as the instance's Git version and if certain features are enabled such as `container_registry_enabled`.

### Integration settings

These are integrations your GitLab instance interacts with such as an [external storage provider](https://docs.gitlab.com/ee/administration/static_objects_external_storage.html) or an [external container registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#use-an-external-container-registry-with-gitlab-as-an-auth-endpoint). These services must be able to send data back into a GitLab instance for data to be tracked.

### Reporting level

Our reporting levels of aggregate or individual reporting varies by segment. For example, on Self-Managed Users, we can report at an aggregate user level using Usage Ping but not on an Individual user level.

### Reporting time period

Our reporting time periods varies by segment. For example, on Self-Managed Users, we can report all time counts and 28 day counts in Usage Ping.

## Metrics Dictionary

The Metrics Dictionary is a single source of truth for the metrics and events we collect for product usage data. The Metrics Dictionary lists all the metrics we collect in Usage Ping, why we're tracking them, and where they are tracked.

The [Metrics Dictionary](https://docs.gitlab.com/ee/development/usage_ping/metrics_dictionary.html) should be updated any time a new metric is implemented, updated, or removed.

We're currently focusing our Metrics Dictionary on [Usage Ping](https://docs.gitlab.com/ee/development/usage_ping.html). In the future, we will also include [Snowplow](https://docs.gitlab.com/ee/development/snowplow/index.html). We currently have an initiative across the entire product organization to complete the [Metrics Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/5425).

## Implementing Product Performance Indicators

We've recently had a large push across the product organization to become more data driven. Part of this push includes getting product metrics in place for each product section, stage, and group. In FY21-Q3 OKRs, we setup a couple of OKRs to help us accomplish this:

- [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343).

To accomplish these OKRs, we setup a seven step process to implement product metrics. This process was originally presented in the Weekly Product Meeting on August 11, 2020 ([slide deck](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit) and [video presentation](https://youtu.be/1l1ru7k-q2I?t=375)) and has been refined over time.

| Implementation Status | Description | Responsibility | Exit Criteria |
| --------------------- | ----------- | -------------- | ------------- |
| [Definition](#definition) | The definition step outlines the process for deciding which product metrics to track. | PM Responsibility, Product Intelligence Support | Metric is defined in the Event Dictionary and [in the Performance Indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/performance_indicators) with the future `metric_name`. Issue for instrumentation is completed and scheduled for the current release. |
| [Instrumentation](#instrumentation) | The instrumentation step outlines how each product team implements data collection. | PM Responsibility, Product Intelligence Support | Instrumentation is completed and feature flags are turned off so that data can be collected |
| [Data Availability](#data-availability) | The data availability step outlines the timing of a product release to receiving product usage data in the data warehouse. | PM Responsibility, Product Intelligence Support | PM confirms that the `metric_name` is present in the data set, updates the PI file with a cc to `@gitlab-org/growth/product-intelligence/engineers @gitlab-data` to inform them that the metric is available. For example, for self-managed usage ping implementation, check the #g_product_intelligence slack channel for the latest SaaS Usage Ping Payload. |
| [Dashboard](#dashboard) | The dashboarding step outlines how Sisense dashboards are built. | PM Responsibility, Product Intelligence Support | There is a chart in Sisense. |
| [Handbook](#handbook) | The [Product PI handbook page](/handbook/product/performance-indicators/#how-to-work-with-pages-like-this) describes how product performance indicators are added for each product section, stage, and group. | PM Responsibility, Product Intelligence Support | Chart is embedded into the handbook. Target has been assigned based off the data. |
| [Target](#target) | The target definition step outlines how targets are defined for each performance indicator. | PM Responsibility, Product Intelligence Support | The target value is in both the chart and in the Performance Indicator (PI) file. |
| [Complete](#complete) | All of the prior steps have been completed. | PM Responsibility, Product Intelligence Support | :tada: |

### 1. Definition

Determine what metrics are important for your specific section, stage, or group.

**Instructions:**

1. Determine the level at which the metric should be measured:
    1. All product level - TMAU, Paid TMAU
    1. Stage level - SMAU, Paid SMAU, Other PI
    1. Group level - GMAU, Paid GMAU, Other PI
1. Define the metric. This is typically done by selecting an initial AMAU that is most representative of overall stage/group use
1. Determine the right [collection framework](/direction/product-intelligence-guide/#collection-framework) to utilize. Some guidance for considering collection frameworks:
    - **Usage Ping - General** - There are many frameworks that report via Usage Ping. All of them have various capabilities regarding types of events and available segementation. All Usage Ping collection frameworks have a frequency of monthly for self-managed usage and every-other-week for SaaS usage.
    - **Snowplow** - Investment in instrumentation should be discouraged - there is already great URL and path tracking from SnowPlow for Gitlab.com. If additional tracking is needed on front-end events consider utilizing Usage Ping Redis HLL. Snowplow instrumentation becomes immediately available on GitLab.com only.
    - **Usage Ping - Redis HLL** - Redis HLL can track distinct counts, typically in front-end metrics on both the client (Javascript) and server (Ruby) side. Redis HLL does not provide session level segmentation on GitLab.com. As Usage Ping instrumentation it is returned from both SaaS and Self-Managed.
    - **Usage Ping - Postgres** - Our original Usage Ping implementation this collects distinct database counts.
    - **Usage Ping - Prometheus** - Note there is no future plan to increase the segmentation of this collection framework and it is presently limited to Instance wide metrics.
1. Plan your future events and metrics with your engineering team and add them to the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485).
1. Add the value from column A of the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) to the [Performance Indicator (PI) yaml file as the `metric_name`](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/performance_indicators)

#### Deduplication of Aggregated Metrics

Note: We now enable you to deduplicate aggregated metrics implemented via Redis HLL, in order to get distinct counts (ex distinct users count across multiple actions in a stage). Please read our docs on [Aggregated Metrics](https://docs.gitlab.com/ee/development/usage_ping/#aggregated-metrics) for more information. We are working towards the ability to deduplicate across multiple Database HLL metrics via [#288848](https://gitlab.com/gitlab-org/gitlab/-/issues/288848), and then deduplication across multiple Redis HLL and Database HLL metrics via [#421](https://gitlab.com/gitlab-org/product-intelligence/-/issues/421)

| Term | Definition | Example |
| ---- | ---------- | ------- |
| Aggregated | Metric contains rolled-up values due to an aggregate function (COUNT, SUM, etc) | Total Page Views (TPV) - the sum of all events when a page was viewed. |
| Deduplicated | Metric counts each unit of measurement once. | Unique Monthly Active Users (UMAU) - each user_id is counted once |
| Deduplicated Aggregated | Metric contains a rolled-up value where each unit is counted once. | UMAU is a deduplicated aggregated metric but TPV is not. |

### 2. Instrumentation

Work with your engineering team to instrument tracking for your XMAU. Focus on using Usage Ping as your metrics will be available on both SaaS and self-managed.

The [Usage Ping Guide](https://docs.gitlab.com/ee/development/usage_ping/) outlines the steps required for instrumentation. It includes:

- [What is Usage Ping?](https://docs.gitlab.com/ee/development/usage_ping/#what-is-usage-ping)
- [How Usage Ping works](https://docs.gitlab.com/ee/development/usage_ping/#how-usage-ping-works)
- [Implementing Usage Ping](https://docs.gitlab.com/ee/development/usage_ping/#implementing-usage-ping)
- [Developing and Testing Usage Ping](https://docs.gitlab.com/ee/development/usage_pin/#developing-and-testing-usage-ping)
- [Example Payload](https://docs.gitlab.com/ee/development/usage_ping/#example-usage-ping-payload)

Also see the [Product Intelligence Guide](https://about.gitlab.com/handbook/product/product-intelligence-guide/) and [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html).

**Instructions:**

1. Read the docs and work with your engineers on instrumentation.
1. Ask for a [Product Intelligence Review](https://docs.gitlab.com/ee/development/usage_ping/#9-ask-for-a-product-intelligence-review) in your MR.

### 3. Data Availability

Plan instrumentation with sufficient lead time for data availability. Ensure your metrics make it into the self-managed release as early as possible.

**Timeline:**

1. [Self-managed releases](https://about.gitlab.com/upcoming-releases/) happen on the 22nd of each month (+30 days)
1. Wait one week for customers to upgrade to the new release and for a Usage Ping to be generated (+7 days)
1. Usage Pings are collected in the Versions application. The Versions application's database is automatically imported into the Snowflake Data Warehouse every day (+1 day).

In total, plan for up to 38 day cycle times (Examples [1](https://gitlab.com/gitlab-data/analytics/-/issues/5629#note_389671640), [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31785#note_392882428)). Cycle times are slow with monthly releases and weekly pings, so, implement your metrics early.

**Instructions:**

1. Merge your metrics into the next self-managed release as early as possible.
1. Wait for your metrics to be released onto production GitLab.com. These releases currently happen on a daily basis.
1. Usage Pings are generated on GitLab.com on a weekly basis. Monitor the #g_product_intelligence slack channel where the Product Intelligence team will post the latest GitLab.com Usage Ping ([example](https://gitlab.slack.com/files/ULXG09FAL/F01905UPPL0/12-gitlab.com-usage-data-for2020-08-04.json?origin_team=T02592416&origin_channel=CL3A7GFPF)). Verify your new metrics are present in the GitLab.com Usage Ping payload.
1. Wait for the Versions database to be imported into the data warehouse.
1. Check the dbt model [version_usage_data_unpacked](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked#columns) to ensure the database column for your metric is present.
1. Check [Sisense](http://app.periscopedata.com/app/gitlab/) to ensure data is available in the data warehouse.

### 4. Dashboard

Dashboard the metric. This is done by creating a Sisense dashboard. Avoid cumulative views and instead focus on month-over-month growth. Instructions for creating dashboards are here.

We need PMs to self-serve their own dashboards as data team capacity is limited. The data team will be focused on enabling self-service, advising PMs, and working on the more challenging XMAU dashboards.

To learn how to create your own dashboard, see [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)

To update the SMAU Summary Dashboards: [GitLab.com Postgres SMAU Dashboard (SaaS)](https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU) and [Usage Ping SMAU Dashboard (SaaS + Self-Managed)](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard), please open a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues).

**Dashboard Prioritization**

For GMAU and SMAU data issues:

- In Q3, due to very limited data team capacity, the data team capacity will be reserved primarily for GMAU and SMAU.
    - Please add `XMAU` label to the data issues.
    - Please limit the ask to XMAU only, rather than all the supporting metrics.
- If there is still need to prioritize within GMAU issues, we will work with Scott and Section Leaders on the prioritization.
    - Section Leaders are encourage to [rank XMAU Issues](https://gitlab.com/gitlab-data/analytics/-/issues/5664#note_392529098) on the [Product Section Board](https://gitlab.com/gitlab-data/analytics/-/boards/1921369?label_name%5B%5D=Product).

For non-GMAU and non-SMAU data issues:

- In Q3, the data team will have very limited capacity to support non-GMAU and non-SMAU data requests.
    - Please continue to create data issues and label Data issues with the appropriate [Product Section Label](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=section).
- If there are critical or urgent data asks, please @hilaqu and your section leader in the issue, with an explanation of why. We will evaluate them on a case-by-case basis.

**Instructions:**

1. Read through [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)
1. Self-serve your dashboard
1. If you need help, create a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues) and ask your Section Leader to prioritize.

### 5. Handbook

There are five Product PI pages: The [Product Team page](https://about.gitlab.com/handbook/product/performance-indicators/) and section pages for [Dev](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/), [Ops](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/), [Sec](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/), [Enablement](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/).

We need all PMs to ensure their PIs are showing on the performance indicator pages. Based off [What we're aiming for](#what-were-aiming-for)

To do so, we need a clear way to communicate to PMs exactly which PIs are remaining. We will be [adding placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906) into the [performance indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) so that all required PIs show in the handbook. Once a PI is implemented, the actual PI will replace the placeholder PI.For more information about how PIs and XMAUs are related to one another, see [PI Structure](https://about.gitlab.com/handbook/product/performance-indicators/#structure).

**Instructions:**

1. @jeromezng @kathleentam will [add placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906).
1. @jeromezng @kathleentam will then meet with each Section, Stage, and Group, to understand the implementation status of each PI and document any exceptions. Exceptions for PIs will then be signed off by Scott, Anoop, and the Section Leaders.
1. Keep your [performance indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) up to date as the implementation status changes.

### 6. Target

As a product organization, we need to get into the habit of understanding our baselines and setting targets for each stage & group. For the PI Target step, you will work with your Section or Group Leader to define targets for each of your XMAUs.

Set a growth target, and embed in the tracking dashboard. Growth targets should be ambitious but achievable.

**Instructions:**

1. Work with your Section or Group Leader to define a target.
1. Add the Target Line into your dashboard ([example](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8490496)).
1. If you need help, book a meeting with @jeromezng @kathleentam.

### 7. Complete

All of the prior steps have been completed and a PI is successfully implemented.

**Instructions**

1. Continually monitor your results, and refine your priorities based on results.
1. Line up your roadmap priorities to positively influence the adoption funnel, drive the performance metric, and hit the growth targets.
1. Deeply understand the drivers of your metric through customer interviews and other product usage data. Visualize this through a user adoption funnel that describes how users enjoy the value of the particular product area you are measuring.

## Quick Links

| Resource | Description |
| -------- | ----------- |
| [Product Intelligence Guide](/handbook/product/product-intelligence-guide) | A guide to Product Intelligence |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/usage_ping/) | An implementation guide for Usage Ping |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html) | An implementation guide for Snowplow |
| [Event Dictionary](/handbook/product/product-intelligence-guide#metrics-dictionary) | A SSoT for all collected metrics and events |
| [Privacy Policy](/privacy/) | Our privacy policy outlining what data we collect and how we handle it |
| [Implementing Product Performance Indicators](/handbook/product/product-intelligence-guide#implementing-product-performance-indicators) | The workflow for putting product performance indicators in place |
| [Product Intelligence Direction](/direction/product-intelligence/) | The roadmap for Product Intelligence at GitLab |
| [Product Intelligence Development Process](/handbook/engineering/development/growth/product-intelligence/) | The development process for the Product Intelligence groups |
