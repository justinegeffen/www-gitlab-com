Issues are refined and weighted prior to scheduling them to an upcoming milestone. We use `candidate::` scoped labels to help with planning work in future iterations. The additional label allows us to filter on the issues we are planning, allowing Product, Engineering and UX to [start async issue refinement on the 4th](/handbook/engineering/workflow/#product-development-timeline).  Weighting also helps with capacity planning with respect to how issues are scheduled in future milestones. 

We create 2 issues as part of our Planning process:
 1. Planning issue - Product is the DRI in prioritizing work, with input from Engineering, UX and Technical Writers. This issue is used to discuss scheduling and team capacity. We use an issue list filtered on the group and candidate labels.
 1. Needs Weight issue - Engineering is the DRI in ensuring issues are refined asynchronously and weighted accordingly, given what is planned for the milestone. This helps Product understand how much can be scheduled in the future milestone(s), once the complexity and effort of the issues are better understood.  For example, issues may need to be broken down if they are too large for a milestone, or further research may be necessary, so subsequent issues or epics can be created.

Both issues currently rely on the `candidate::` scoped label to determine what issues are to be investigated for the upcoming milestone(s).

#### How Engineering Refines Issues

*side note: we prefer [using Refining over Grooming](/handbook/communication/#top-misused-terms)*

Engineers are expected to allocate approximately 4 hours each milestone to refine and weight issues assigned to them. The purpose of refining an issue is to ensure the problem statement is clear enough to provide a rough effort sizing estimate; the intention is not to provide **solution validation** during refinement.

Engineering uses the [following handbook guidance for determining weights](#weighting-issues). If any issue needs any additional `~frontend ~backend ~Quality ~UX ~documentation` reviews, they are assigned to the respective individual(s). 

##### Checklist for Refining Issues

1. Does the issue have a problem statement in the description?
1. Does the issue have the expected behaviour described well enough for anyone to understand?
1. Does the issue explicitly define who the stakeholders are (e.g. BE, FE, PM, UX and/or Tech Writer)?
1. Does the issue have a proposal in the description? *If so:*
    1. Does the proposal address the problem statement?
    1. Are there any unintended side effects of the implementation?
1. Does the issue have proper labeling matching the job to be done? (e.g. bug, feature, performance)

Any one on the team can contribute to answering the questions in this checklist, but the final decisions are up to the PM and EMs.

##### Steps for Refining and Weighting Issues

Engineers will:
1. Go through the checklist above for refining issues assigned to them
1. Add a [weight based on the definitions](#weighting-issues)
1. Update the `~workflow::` label to the appropriate status, e.g. 
   * ~"workflow::design" if further design refinement is needed, and let the designer know
   * ~"workflow::ready for development" when refinement is completed and a weight has been applied, signaling that it's ready for implementation and the issue can be scheduled accordingly
   * ~"workflow::planning breakdown" if more investigation and research is needed, the status does not move, and the PM and EMs should be pinged
1. Unassign themselves from the issue when they are done refining and weighting the issue
