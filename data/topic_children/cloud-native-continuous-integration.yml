description: Cloud native development needs continuous integration that supports speed. See what separates cloud native CI from regular CI.
canonical_path: /topics/ci-cd/cloud-native-continuous-integration/
parent_topic: ci-cd
file_name: cloud-native-continuous-integration
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: What is cloud native continuous integration?
header_body: In modern software development, most teams are already practicing continuous
  integration (CI). As DevOps teams look to increase velocity and scale, they
  look to cloud computing to help them achieve those goals. This kind of
  development is called cloud native development. These two concepts, CI and
  cloud native, work together so that teams can deploy to different
  environments.
body: >-
  ## Cloud native + continuous integration[](https://about.gitlab.com/topics/ci-cd/cloud-native-continuous-integration/#cloud-native--continuous-integration)


  [Cloud native](https://about.gitlab.com/cloud-native/) is a way to build and run applications that take advantage of the scalability of the cloud computing model. Cloud native computing uses modern cloud services, like container orchestration, serverless, and [multicloud](https://about.gitlab.com/topics/multicloud/) to name a few. Cloud native applications are built to run in the cloud.


  CI is the practice of integrating code into a shared repository and building/testing each change automatically, several times per day. For teams using [pipeline as code](https://about.gitlab.com/topics/ci-cd/pipeline-as-code/), they can configure builds, tests, and deployment in code that is trackable and stored in the same shared repository as their source code.


  Cloud native continuous integration is simply continuous integration that can supports cloud services often used in cloud native development.


  ## What a cloud native CI pipeline needs[](https://about.gitlab.com/topics/ci-cd/cloud-native-continuous-integration/#what-a-cloud-native-ci-pipeline-needs)


  Cloud native offers opportunities in terms of velocity and scale, but also [increases complexity](https://thenewstack.io/the-shifting-nature-of-ci-cd-in-the-age-of-cloud-native-computing/). Cloud native engineering teams need increased automation and stability, and CI/CD tools designed to support the complexity that comes from developing in a [microservices](https://about.gitlab.com/topics/microservices/) environment.


  For better cloud native development, teams should ensure their continuous integration solutions are optimized for the cloud services they commonly use:


  * Container orchestration tools, like [Kubernetes](https://about.gitlab.com/solutions/kubernetes/), allow developers to coordinate the way in which an application’s containers will function, including scaling and deployment. For teams using Kubernetes, their cloud native CI should have a robust Kubernetes integration to support adding and/or managing multiple clusters.

  * Seamless [continuous delivery](https://about.gitlab.com/stages-devops-lifecycle/continuous-delivery/) (CD), in addition to continuous integration, is important for cloud native and microservices development. High-functioning deployment strategies, like [canary deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html), can help cloud native teams test new features with the same velocity they use to build them.

  * Cloud native applications are often architectured using microservices instead of a monolithic application structure, and rely on containers to package the application’s libraries and processes for deployment. A cloud native CI tool with [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html) can help streamline this process.


  Cloud native continuous integration is designed to support the cloud services and architectures cloud native teams use, and offers the automation teams need for speed and stability.
cta_banner:
  - cta:
      - text: Learn More
        url: https://docs.gitlab.com/ee/user/project/clusters/
    title: Deploy software from GitLab CI/CD pipelines to Kubernetes
resources_title: More about cloud native development
resources:
  - title: How to deploy on AWS from GitLab
    url: https://about.gitlab.com/resources/whitepaper-deploy-aws-gitlab/
    type: Whitepapers 
suggested_content:
  - url: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
  - url: /blog/2020/12/17/gitlab-for-cicd-agile-gitops-cloudnative/
  - url: /blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/
