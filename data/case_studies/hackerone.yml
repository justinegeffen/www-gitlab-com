title: HackerOne
cover_image: '/images/blogimages/hackerone-cover-photo.jpg'
cover_title: |
  HackerOne achieves 5x faster deployments with GitLab’s integrated security
cover_description: |
  HackerOne improved pipeline time, deployment speed, and developer efficiency with GitLab Ultimate.
twitter_image: '/images/blogimages/hackerone-cover-photo.jpg'

twitter_text: "Discover how the world's most trusted hacker-powered security platform, @Hacker0x01, adopted GitLab to eliminate disparate toolchains and shift security left."

customer_logo: '/images/case_study_logos/hackerone-logo.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: San Francisco, US; London, UK; Groningen, NL
customer_solution: GitLab Ultimate
customer_employees: 300+ employees 
customer_overview: |
  The world's most trusted hacker-powered security company, HackerOne, adopted GitLab to eliminate disparate toolchains and shift security left.
customer_challenge: |
  HackerOne was looking for a platform to ease workflow dependencies and improve developer throughput.

key_benefits:
  - |
    Faster deployments 
  - |
    Shift-left security features
  - |
    Tool consolidation
  - |
    Less context switching
  - |
    AWS integration
  - |
    Cost savings
  - |
    Improved trust in software
  - |
    Single source of truth

customer_stats:
  - stat: 8 min  
    label: Pipeline time down from 60 (7.5x faster)
  - stat: 5x      
    label: Faster deployment time (4-5/day up from 1/day)
  - stat: 4 hours   
    label: Development time per engineer saved/weekly 


customer_study_content:
  - title: the customer
    subtitle: Hacker-powered security platform
    content:
      - |
        HackerOne empowers the world to build a safer internet. As the world’s most trusted hacker-powered security platform, HackerOne gives organizations access to the largest community of hackers on the planet. Armed with the most robust database of vulnerability trends and industry benchmarks, the hacker community mitigates cyber risk by searching, finding, and safely reporting real-world security weaknesses for organizations across all industries and attack surfaces.
      - |
        As the world becomes more connected and organizations move to the cloud, cybersecurity must keep up. [HackerOne](https://www.hackerone.com) helps security teams scale with their agile attack surfaces by providing hacker-powered security testing and vulnerability insights that help reduce risk across the SDLC.

  - title: the challenge
    subtitle:  Looking for speed of development and deployment
    content:
      - |
        HackerOne is a globally distributed company, so dependencies exist between teams in order to complete projects. There were often times when a developer in the Netherlands worked on code, then someone in North America would pick up where the other left off.  Lengthy pipeline times could interrupt handoffs. According to Mitch Trale, Head of Infrastructure at HackerOne, “In many cases, you would wind up stranding a merge request in a place where you wish you would have gone live ... if we'd had faster tools, we could have put that out there.”   
      - |
        HackerOne was using separate tools for code version control and continuous integration. As HackerOne began to scale, growing the engineering team from 10 to 30 members, Mitch indicated that  these tools were “significantly limited...one example of this is just the time it took to run a single pipeline within our old system that made it sort of prohibitive to do this frequently,” Trale said. “So engineers started to work around these limitations. We started creating these downstream side effects, which we then had to deal with separately.” The team needed a tool that could grow alongside HackerOne’s development and would be able to manage multiple projects that would affect multiple squads.
      - |
        Primary drivers for the team in their consideration of new software tools was speed of development, speed of deployment, and developer happiness. “We're optimizing for happy engineers, wherever possible. Better tools help us automate more, providing us with better throughput and higher quality,” Trale clarified. The team needed a platform that would improve the developer experience from end-to-end, from development to deployment.
 
  - blockquote: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
    attribution: Mitch Trale
    attribution_title: Head of Infrastructure, HackerOne 
   
  - title: the solution
    subtitle:  Scalability and developer ownership
    content:
      - |
        HackerOne adopted GitLab in late 2018 for [source management](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/), [issues management](https://about.gitlab.com/stages-devops-lifecycle/plan/), CI/CD, and security and compliance features that didn’t exist in the team’s previous tooling system. GitLab buy-in increased dramatically across the company because of the various agility offerings that can work in different environments. “HackerOne adopted GitLab all around, but we also saw brand new features that product managers and sprint owners could take advantage of. Now we're seeing teams cross-planning out further in the future,” Trale reports. “We're seeing Gantt charts that represent dependencies. And that kind of sophistication was really critical at that stage in HackerOne's development, because we were scaling.” 
      - |
        GitLab not only provided a way for teams to scale, but it also provided a way for application development processes to become more egalitarian. Because of GitLab’s intuitive user interface, [the number of users expanded](https://about.gitlab.com/enterprise/) at HackerOne. “We have technical product managers who can now make code changes affecting copy text on the site or affecting, for example, a font color,” Trale added. “It's easier for individuals to go in using GitLab's visual editing tools, and a merge request that can easily be approved and deployed atomically. That simply wasn't viable before.”
      - |
        GitLab’s ease of use makes working within the platform more manageable for developers and engineers. As an open source tool, GitLab is modern, but not overly complex. It offers a lot of capabilities, but it is functionally available to engineers at every level of the company. “Now we can democratize control over our pipelines. We can have individual engineers acting as DevOps, acting as infrastructure... and administering the tooling in a way that they simply couldn't before. Our old tooling was clunky, hard to maintain and manage,” Trale said.
          
  - title: the results
    subtitle: Baked-in security generates faster deploys
    content:
      - |
        One of the biggest benefits of adopting GitLab is the ability to find code issues sooner in the pipeline. When combined with faster pipelines, the teams can now work iteratively to resolve security flaws. The engineering team used to spend at least 60 minutes per integration pipeline run. It would go end-to-end from commit to test, to smoke test, test, deploy, and take an hour. If there was a single error, they would have to rerun the whole process.
      - |
        “It made people overly cautious about pushing code. And what we really want to do is to make that cycle time as tight as possible and reduce risk associated with any given release of code,” Trale explained. “So GitLab was strategically important for us because it really enabled us to refine this code and build it according to our own quality specifications.”
      - |
        The team can also rerun specific parts of the pipeline, which was not feasible before. They can focus on the part of the continuous integration pipeline that might be failing, without having to restart. “Speed matters … now it takes about eight minutes to run a pipeline. That eight minutes is massive. That alone would have been meaningful enough for us to consider the switch, the promise of this high-speed continuous integration pipeline,” Trale remarked.  On top of that, there is now deeper visibility into audit logs, so they can see what is going on behind the scenes to understand what is contributing to performance degradation.
      - |
        The engineering team created a bot using the GitLab’s API and security capabilities that generates merge requests automatically based on outdated packages. The bot scans repos and creates merge requests according to those that need to be updated. Engineers only need to review and approve them in order to then deploy. This automation saves manual cycle time and creates faster security scanning. It’s no longer an individual’s job to spend an additional hour per deploy testing this. “We deploy code multiple times a day...now at least between one to five times a day, new versions of HackerOne are going live to the web. There was at least an hour spent on each of those by an engineer,” Trale described. “Maybe a half hour between two engineers, making sure that the work made sense. I think, conservatively, we're saving four to five hours a day of energy per engineer by consolidating this work using the tools.”
      - |
        Prior to GitLab, HackerOne’s deployment cycle was closer to one to two times per day. But with everything in one place, correctly labeled and efficiently organized, PMs and those who manage sprints can now pick what they want to work on. “GitLab is helping us catch these things early - it's integrating it into the developer's flow.
      - |
        Having all the tools in one place has made security scanning and audits an easier process from the team's previous workflow. “The cost of running security scans in GitLab is significantly lower than it was previously. And so we're much more inclined to run more thorough scans, faster. Whether that's on individual packages or even running a suite of security tests. I do think that we're much more cognizant of it and we're using GitLab for this purpose,” Trale explained.
      - |
        The engineering team also built a custom Slack bot that integrates with GitLab and triggers deployments. All deployments are public in the Slack channel, where a lot of work happens for HackerOne. With the integration, they can see deployment status in Slack rather than locating the pipeline or audit log. In cases where the deployment goes wrong, there are 30 people who can help debug in real-time. Bringing deployments close to Slack and using GitLab for CI/CD provides easier, faster access to code and security management.
      - |
        While tool consolidation and deployment speed are priorities that led HackerOne to make the switch, it’s GitLab’s active development that continues to impress team members. GitLab has monthly releases that build upon existing features - such as security - using customer feedback. “The partnership that we have with GitLab is ever deepening. Whereas, some of these other tools that we evaluated didn't have that strength of development, that sort of momentum that GitLab has,” Trale said. “The monthly cadence speaks to this - new features arrive frequently that we can take advantage of. That active development is a contemporary mindset that GitLab has, which is appealing to us.”
      - |


      - |
        ## Learn more about GitLab solutions
      - |
        [GitLab for enhancing security](/stages-devops-lifecycle/secure/)
      - |
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
      - |
        [Choose a plan that suits your needs](/pricing/)


